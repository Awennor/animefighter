using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class PauseManagerClip : PlayableAsset, ITimelineClipAsset
{
    public PauseManagerBehaviour template = new PauseManagerBehaviour ();

    public ClipCaps clipCaps
    {
        get { return ClipCaps.None; }
    }

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<PauseManagerBehaviour>.Create (graph, template);
        PauseManagerBehaviour clone = playable.GetBehaviour ();
        return playable;
    }
}

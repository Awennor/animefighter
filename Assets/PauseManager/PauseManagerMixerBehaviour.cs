using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class PauseManagerMixerBehaviour : PlayableBehaviour
{
    private PauseManager trackBinding;

    // NOTE: This function is called at runtime and edit time.  Keep that in mind when setting the values of properties.
    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        trackBinding = playerData as PauseManager;
        //Debug.Log("PROCESS FRAME!");
        if (!trackBinding) {
            Unpause();
            return;
        }

        //Debug.Log("PROCESS FRAME!2");
        int inputCount = playable.GetInputCount ();
        bool pause = false;
        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);
            ScriptPlayable<PauseManagerBehaviour> inputPlayable = (ScriptPlayable<PauseManagerBehaviour>)playable.GetInput(i);
            PauseManagerBehaviour input = inputPlayable.GetBehaviour ();
            if (inputWeight > 0) {
                //Debug.Log("INPUT WEIGHT0" + inputWeight);
                pause = true;
            }
            // Use the above variables to process each frame of this playable.
            
        }
        //Debug.Log("PROCESS FRAME!2"+pause);
        if (pause)
        {
            trackBinding.AddFlag(PauseFlags.BATTLECUTSCENE);
        }
        else {
            
            Unpause();
        }
    }

    public override void OnGraphStop(Playable playable)
    {
        if (trackBinding != null)
            Unpause();
    }

    private void Unpause()
    {
        //Debug.Log("unpause");
        trackBinding.RemoveFlag(PauseFlags.BATTLECUTSCENE);
    }
}

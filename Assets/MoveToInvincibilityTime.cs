﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToInvincibilityTime : MonoBehaviour {

    public DigitalPosition digitalPosition;
    public InvincibilityTime invincibilityTime;
    public float time;

	// Use this for initialization
	void Start () {
        digitalPosition.OnPositionChanged += DigitalPosition_OnPositionChanged;
	}

    private void DigitalPosition_OnPositionChanged(int obj)
    {
        invincibilityTime.InvincibleState(time);
    }

    // Update is called once per frame
    void Update () {
		
	}
}

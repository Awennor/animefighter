﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingDataHolder : MonoBehaviour {

    [SerializeField]
    DataUnit[] dataunits;

    public DataUnit[] Dataunits
    {
        get
        {
            return dataunits;
        }

        set
        {
            dataunits = value;
        }
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    [Serializable]
    public class DataUnit{
        [SerializeField]
        ShootingData data;

        public ShootingData Data
        {
            get
            {
                return data;
            }

            set
            {
                data = value;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VisualDebugging {

    static ParticleSystem particleSystem;
    static ParticleSystem prefab;

    public static string filePath = "debugparticle";

    public static void DebugRed(Vector3 pos) {
        DebugColor(pos, Color.red);
    }

    public static void DebugBlue(Vector3 pos)
    {
        DebugColor(pos, Color.blue);
    }

    public static void DebugColor(Vector3 pos, Color c)
    {
        if (particleSystem == null) {
            if(prefab == null)
                prefab = (Resources.Load(filePath) as GameObject).GetComponent<ParticleSystem>();
            particleSystem = GameObject.Instantiate(prefab);
            
            
            //var cameraFor = mainCamera.transform.forward;

            
        }
        var mainCamera = Camera.main;
        var screenPoint = mainCamera.WorldToScreenPoint(pos);
        //screenPoint.z = 1;
        //Debug.Log(screenPoint);
        var worldPoint = mainCamera.ScreenToWorldPoint(screenPoint);
        particleSystem.transform.position = worldPoint - mainCamera.transform.forward * 2;
        particleSystem.Emit(1);
    }

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollisionSystem : MonoBehaviour
{

    [SerializeField]
    public List<DigitalPosition> digitalPositions;
    [SerializeField]
    List<CollisionSchedule> scheduledCollisions = new List<CollisionSchedule>();
    [SerializeField]
    List<CollisionSchedule> activeCollisions = new List<CollisionSchedule>();
    public UnityEvent OnCollisionHappened;
    [SerializeField]
    public List<UnityEvent> OnCollisionActivePosition;
    [SerializeField]
    public List<UnityEvent> OnCollisionInactivePosition;
    public event Action<int> OnCollisionHappened_Id;

    float collisionTooOldThreeshold = 0.5f;
    
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float time = Time.time;
        bool removeFromActive = false;

        for (int i = 0; i < activeCollisions.Count; i++)
        {
            var c = activeCollisions[i];
            if (time <= c.EndTime)
            {
                
                removeFromActive = RunCollisionCheck(c);

            }
            else
            {
                removeFromActive = true;
            }
            if (removeFromActive)
            {
                i--;
                DeactivateCollision(c);
            }
        }

        for (int i = 0; i < scheduledCollisions.Count; i++)
        {
            var c = scheduledCollisions[i];
            removeFromActive = false;
            if (time > c.StartTime)
            {
                activeCollisions.Add(c);
                //test if collision is older than the threeshold and ended a long time ago
                if (time > (c.EndTime + collisionTooOldThreeshold))
                {
                    removeFromActive = true;
                }
                else {
                    removeFromActive = RunCollisionCheck(c);
                    int pos = c.Pos;
                    OnCollisionActivePosition[pos].Invoke();
                }

                scheduledCollisions.Remove(c);
                
                i--;
            }
            if (removeFromActive)
            {
                DeactivateCollision(c);
            }
        }

    }

    private void DeactivateCollision(CollisionSchedule c)
    {
        activeCollisions.Remove(c);
        int pos = c.Pos;
        OnCollisionInactivePosition[pos].Invoke();
    }

    private bool RunCollisionCheck(CollisionSchedule c)
    {
        bool collidedB = false;
        foreach (var d in digitalPositions)
        {
            InvincibilityTime invincibilityTime = d.GetComponent<InvincibilityTime>();
            bool invincible = invincibilityTime != null ? invincibilityTime.Invincible : false;
            if (d.GetComponent<Hittable>().Active 
                && d.GetComponent<Health>().AboveZero() 
                && !invincible)
            {
                if (d.Position == c.Pos)
                {
                    collided(d, c);
                    collidedB = true;
                }
            }
            
        }
        return collidedB;
    }



    internal int ScheduleCollision(int position, float delay, float timeColliding)
    {
        float startTime;
        float endTime;
        startTime = Time.time + delay;
        endTime = startTime + timeColliding;
        int pos = position;

        var c = new CollisionSchedule(pos, startTime, endTime);
        scheduledCollisions.Add(c);
        return c.Id;
    }

    private void collided(DigitalPosition d, CollisionSchedule c)
    {
        d.gameObject.GetComponent<Health>().HealthChange(-1);
        //VisualDebugging.DebugRed(d.transform.position);
        OnCollisionHappened.Invoke();
        if (OnCollisionHappened_Id != null) {
            OnCollisionHappened_Id(c.Id);
        }
    }
}

[Serializable]
public struct CollisionSchedule
{
    [SerializeField]
    private int pos;
    [SerializeField]
    private float startTime;
    [SerializeField]
    private float endTime;
    [SerializeField]
    int id;
    static int idAssigner;

    public CollisionSchedule(int pos, float startTime, float endTime)
    {
        this.pos = pos;
        this.startTime = startTime;
        this.endTime = endTime;
        idAssigner++;
        this.id = idAssigner;
    }

    public int Pos
    {
        get
        {
            return pos;
        }
    }

    public float StartTime
    {
        get
        {
            return startTime;
        }
    }

    public float EndTime
    {
        get
        {
            return endTime;
        }
    }

    public int Id
    {
        get
        {
            return id;
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsEraser : MonoBehaviour {

	// Use this for initialization
	void Awake() {
        if (enabled) {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();
        }
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

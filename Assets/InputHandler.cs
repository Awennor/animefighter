﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputHandler : MonoBehaviour {

    public UnityEvent ButtonPressed;
    public string InputKey;
    public KeyCode[] InputKey_Code;

    void Update()
    {
        if (InputKey.Length > 0 && Input.GetButtonDown(InputKey))
        {
            KeyPressed();
        }
        for (int i = 0; i < InputKey_Code.Length; i++)
        {
            var key = this.InputKey_Code[i];
            if (Input.GetKeyDown(key))
            {
                KeyPressed();
                return;
            }
        }
        
    }

    private void KeyPressed()
    {
        ButtonPressed.Invoke();
    }

    public void ForceKeyPress() {
        KeyPressed();
    }

    // Use this for initialization
    void Start () {
		
	}
	
}

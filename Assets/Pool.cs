﻿using System;
using System.Collections.Generic;


public class Pool<T>
{
    List<T> active = new List<T>();
    List<T> inactive = new List<T>();

    public bool HasActive { get { return active.Count > 0; } }
    public bool HasInactive { get { return inactive.Count > 0; } }

    internal void Desactive(int v)
    {
        for (int i = 0; i < v; i++)
        {
            DesactivateObject(active[i]);
        }
        
    }

    public virtual T NewInstance() {
        return default(T);
    }

    public T ActivateObject() {
        T t = default(T);
        if (inactive.Count > 0)
        {

            int index = inactive.Count - 1;
            t = inactive[index];
            inactive.RemoveAt(index);
            
            
        }
        else {
            t = NewInstance();
        }
        active.Add(t);
        return t;
    }

    public void DesactivateObject(T t) {
        active.Remove(t);
        inactive.Add(t);
    }

    public void DesactivateAll()
    {
        
        inactive.AddRange(active);
        active.Clear();

    }

    public List<T> Active
    {
        get
        {
            return active;
        }

        set
        {
            active = value;
        }
    }

    public List<T> Inactive
    {
        get
        {
            return inactive;
        }

        set
        {
            inactive = value;
        }
    }
}


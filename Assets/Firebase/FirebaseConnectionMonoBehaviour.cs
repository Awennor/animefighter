﻿using SimpleFirebaseUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirebaseConnectionMonoBehaviour : MonoBehaviour {
    FirebaseConnectionManager connectionManager;
    public string firebaseSecret;
    public string firebaseURL;

    [ContextMenu("Init")]
    public void InitFire() {
        connectionManager.InitFire();
    }


    // Use this for initialization
    void Awake() {
        connectionManager = new FirebaseConnectionManager(firebaseSecret, firebaseURL);

    }

    // Update is called once per frame
    void Update() {

    }
    public void SaveInfo(string content, string[] path)
    {
        connectionManager.SaveInfo(content, path);
    }

    public void SaveInfo(string content, string path)
    {
        connectionManager.SaveInfo(content, path);

    }

}

﻿using SimpleFirebaseUnity;
using UnityEngine;

public class FirebaseConnectionManager
{
    private Firebase firebase;
    private string firebaseSecret;
    private string firebaseURL;

    public FirebaseConnectionManager(string firebaseSecret, string firebaseURL)
    {
        this.firebaseSecret = firebaseSecret;
        this.firebaseURL = firebaseURL;
    }

   
    public void InitFire()
    {

        if (firebase == null)
        {
            //firebase = Firebase.CreateNew("cubeproto.firebaseio.com", "eeYFCVruCyhqIOSDbntSaEhPKUFC2fToujCXrR95");
            firebase = Firebase.CreateNew(firebaseURL, firebaseSecret);
        }
        //firebase = Firebase.CreateNew(firebaseURL, firebaseSecret);
        //firebase = Firebase.CreateNew("secretmachine-fb281.firebaseio.com", "DAijFiMTAvTAxeLOkc5bPN78PC2ZQfT7i5oAF2OH");

        firebase.OnSetSuccess += OnSetSuccess;
        firebase.OnSetFailed += OnSetFail;
    }

    public void SaveInfo(string content, string[] path)
    {
        if (this.firebase == null)
        {
            InitFire();
        }
        var firebase = this.firebase;
        for (int i = 0; i < path.Length; i++)
        {
            firebase = firebase.Child(path[i], true);
        }
        firebase.SetValue(content);
    }

    public void SaveInfo(string content, string path)
    {
        if (this.firebase == null)
        {
            InitFire();
        }
        var firebase = this.firebase;
        firebase = firebase.Child(path, true);
        firebase.SetValue(content);

    }

    private void OnSetFail(Firebase arg1, FirebaseError arg2)
    {
        Debug.Log("SET FAIL!!!" + arg2.Message + arg2.InnerException);
    }

    private void OnSetSuccess(Firebase arg1, DataSnapshot arg2)
    {
        Debug.Log("SET SUCCESS!!!");
    }
}

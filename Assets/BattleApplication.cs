﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleApplication : MonoBehaviour {

    public GameManager gameManager;
    public HeroDeath heroDeath;
    public event Action OnHeroDeath, OnVictory;
    [SerializeField]
    bool heroAutoShoot;

    public bool HeroAutoShoot
    {
        get
        {
            return heroAutoShoot;
        }

        set
        {
            heroAutoShoot = value;
        }
    }

    // Use this for initialization
    void Start () {
        CentralSingleton.Instance.BattleApplicationStarted(this);

        gameManager.OnVictory_AnimationEnded += this.OnVictory;
        heroDeath.OnHeroDeath += OnHeroDeath;
        gameManager.HeroAutoShoot(heroAutoShoot);

	}

    public void ToggleAutoShoot() {
        heroAutoShoot = !heroAutoShoot;
        gameManager.HeroAutoShoot(heroAutoShoot);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

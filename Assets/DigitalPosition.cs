﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DigitalPosition : MonoBehaviour {

    int position;
    public event Action<int> OnPositionChanged;

    public int Position
    {
        get
        {
            return position;
        }

        set
        {
            position = value;
            if (OnPositionChanged != null)
                OnPositionChanged(position);
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

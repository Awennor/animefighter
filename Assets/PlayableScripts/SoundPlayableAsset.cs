﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[System.Serializable]
public class SoundPlayableAsset : PlayableAsset
{
    public SoundPlayable soundPlayable;
    public ExposedReference<AudioSource> audioSource;

	// Factory method that generates a playable based on this asset
	public override Playable CreatePlayable(PlayableGraph graph, GameObject go) {
        
        ScriptPlayable<SoundPlayable> scriptPlayable = ScriptPlayable<SoundPlayable>.Create(graph, soundPlayable);
        scriptPlayable.GetBehaviour().Source = audioSource.Resolve(graph.GetResolver());

        return scriptPlayable;

		
	}
}

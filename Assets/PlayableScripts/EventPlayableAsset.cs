﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[System.Serializable]
public class EventPlayableAsset : PlayableAsset
{
    public EventPlayable soundPlayable;
    public ExposedReference<EventHolder> eventSource;

	// Factory method that generates a playable based on this asset
	public override Playable CreatePlayable(PlayableGraph graph, GameObject go) {
        
        ScriptPlayable<EventPlayable> scriptPlayable = ScriptPlayable<EventPlayable>.Create(graph, soundPlayable);
        scriptPlayable.GetBehaviour().Source = eventSource.Resolve(graph.GetResolver());

        return scriptPlayable;

		
	}
}

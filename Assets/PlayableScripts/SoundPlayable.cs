﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

// A behaviour that is attached to a playable
[Serializable]
public class SoundPlayable : PlayableBehaviour
{


    //public AudioClip clip;
    AudioSource source;
    float volume;
    bool played;
    public bool debugMessages;

    public AudioSource Source
    {
        get
        {
            return source;
        }

        set
        {
            source = value;
        }
    }

    // Called when the owning graph starts playing
    public override void OnGraphStart(Playable playable)
    {
        if (debugMessages)
        {
            Debug.Log("SoundPlayable: Played clip " + source.clip.name);
        }
    }

    // Called when the owning graph stops playing
    public override void OnGraphStop(Playable playable)
    {

    }

    // Called when the state of the playable is set to Play
    public override void OnBehaviourPlay(Playable playable, FrameData info)
    {
        //AudioSource.PlayClipAtPoint(clip, new Vector3(0,0,0));
        //Debug.Log("play");
        TryPlay();
    }

    // Called when the state of the playable is set to Paused
    public override void OnBehaviourPause(Playable playable, FrameData info)
    {
        played = false;
    }

    // Called each frame while the state is set to Play
    public override void PrepareFrame(Playable playable, FrameData info)
    {
        TryPlay();
    }

    private void TryPlay()
    {
        if (!played)
        {
            if (Source == null)
            {
                //AudioSource.PlayClipAtPoint(clip, new Vector3(0, 0, 0));
            }
            else
            {
                //if(clip!= null)
                //Source.clip = clip;
                if (debugMessages)
                {
                    Debug.Log("SoundPlayable: Played clip " + source.clip.name);
                }
                Source.Play();
            }

            played = true;
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseManager : MonoBehaviour {

    public List<PauseFlags> activeFlags;
    public List<GameObject> objectsForPausing;
    private bool lastPause;

    public void AddFlag(PauseFlags f) {
        if(!activeFlags.Contains(f))
            activeFlags.Add(f);
        UpdatePause();
    }

    private void UpdatePause()
    {
        bool pause = activeFlags.Count != 0;
        if (pause != lastPause) {
            Debug.Log("UPDATE PAUSE" + pause);
            for (int i = 0; i < objectsForPausing.Count; i++)
            {
                objectsForPausing[i].SetActive(!pause);
            }
        }
        lastPause = pause;
    }

    public void RemoveFlag(PauseFlags f)
    {
        activeFlags.Remove(f);
        UpdatePause();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
	}
}

public enum PauseFlags {
    BATTLECUTSCENE
}

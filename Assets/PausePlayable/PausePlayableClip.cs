using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class PausePlayableClip : PlayableAsset, ITimelineClipAsset
{
    public PausePlayableBehaviour template = new PausePlayableBehaviour ();
    public ExposedReference<PauseManager> PauseSystem;

    public ClipCaps clipCaps
    {
        get { return ClipCaps.None; }
    }

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<PausePlayableBehaviour>.Create (graph, template);
        PausePlayableBehaviour clone = playable.GetBehaviour ();
        clone.pauseSystem = PauseSystem.Resolve (graph.GetResolver ());
        return playable;
    }
}

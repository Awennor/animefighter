using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class PausePlayableMixerBehaviour : PlayableBehaviour
{
    // NOTE: This function is called at runtime and edit time.  Keep that in mind when setting the values of properties.
    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        int inputCount = playable.GetInputCount();
        float totalWeight = 0;
        bool pause = false;
        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);

            ScriptPlayable<PausePlayableBehaviour> playableInput = (ScriptPlayable<PausePlayableBehaviour>)playable.GetInput(i);
            PausePlayableBehaviour input = playableInput.GetBehaviour();
            var pauseSystem = input.pauseSystem;
            if (pauseSystem != null)
            {
                if (inputWeight > 0)
                {
                    pauseSystem.AddFlag(PauseFlags.BATTLECUTSCENE);
                }
                else
                {
                    pauseSystem.RemoveFlag(PauseFlags.BATTLECUTSCENE);
                }
            }

        }

        //if (inputCount > 0)
        //{
        //    int i = 0;
        //    float inputWeight = playable.GetInputWeight(i);
        //    ScriptPlayable<PausePlayableBehaviour> inputPlayable = (ScriptPlayable<PausePlayableBehaviour>)playable.GetInput(i);
        //    PausePlayableBehaviour input = inputPlayable.GetBehaviour();
        //    input.pauseSystem.AddFlag(PauseFlags.BATTLECUTSCENE);
        //}
        //else
        //{
        //    input.pauseSystem.AddFlag(PauseFlags.BATTLECUTSCENE);
        //}
    }

    public override void OnGraphStop(Playable playable)
    {
        int inputCount = playable.GetInputCount();

        for (int i = 0; i < inputCount; i++)
        {
            ScriptPlayable<PausePlayableBehaviour> playableInput = (ScriptPlayable<PausePlayableBehaviour>)playable.GetInput(i);

            PausePlayableBehaviour input = playableInput.GetBehaviour();
            if (input != null)
            {
                var pauseSystem = input.pauseSystem;
                if (pauseSystem != null)
                {

                    pauseSystem.RemoveFlag(PauseFlags.BATTLECUTSCENE);
                }
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class Behavior : MonoBehaviour {

    [SerializeField]
    private EnemyBehaviourData behaviourData;
    int index = 0;
    public Shooting shooting;
    private IDisposable disposable;
    [SerializeField]
    bool active;
    public ShootingDataHolder shootingData;
    //public event Action<EnemyBehaviourData> behaviorDataChanged;
    public ReactiveProperty<EnemyBehaviourData> ReactiveBehavior { get; private set; }
    public bool autoActive;
    public float afterShootDelay = 0.3f;

    public EnemyBehaviourData BehaviourData
    {
        get
        {
            return behaviourData;
        }

        set
        {
            this.behaviourData = value;
            ReactiveBehavior.Value = value;
            index = 0;
        }
    }

    internal void ActivateAndEnable(bool active)
    {
        this.active = active;
        this.enabled = active;
    }

    public bool Active
    {
        get
        {
            return active;
        }


    }

    private void Awake()
    {
        ReactiveBehavior = new ReactiveProperty<EnemyBehaviourData>();
    }

    private void Start()
    {
        
        if (autoActive) StartActing();
    }

    private void OnEnable()
    {
        if (active) StartActing();
    }

    private void OnDisable()
    {
        
    }



    public void DoAction()
    {
        if (this == null) return;
        if (!active || !enabled || !gameObject.activeInHierarchy) return;
        
        disposable = null;
        //Debug.Log("do action "+index);
        var au = BehaviourData.GetActionUnit(index);
        var bT = au.Bt;
        switch (bT)
        {
            case BehaviourTypes.Shoot:
                {
                    var shootType = au.ArgInt;
                    var position = au.ArgInt2;
                    shooting.ShootIntention(shootingData.Dataunits[shootType].Data, position);
                }
                

                NextAction(afterShootDelay);
                break;

            case BehaviourTypes.Wait:
                NextAction(au.ArgFloat);
                break;
            default:
                break;
        }
    }

    internal void StartActing()
    {
        Debug.Log("start acting");
        active = true;
        index = 0;
        DoAction();
        
        
    }

    public void StopActing() {
        active = false;
        Debug.Log("stop acting");
        if(disposable != null)
            disposable.Dispose();
        disposable = null;
    }

    private void NextAction(float argFloat)
    {
        if (disposable == null) {
            //Debug.Log("next action " + argFloat);
            disposable = Observable.Timer(TimeSpan.FromSeconds(argFloat)).Subscribe((l) => {

                index++;
                if (!BehaviourData.ContainsIndex(index))
                {
                    index = 0;
                    //Debug.Log("Zero index");
                }
                DoAction();
            });
        }
        
    }

    // Update is called once per frame
    void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class ActorView : MonoBehaviour {

    public PlayableDirector heroDamage;
    [SerializeField]
    private HeroMovement movement;

    public HeroMovement Movement
    {
        get
        {
            return movement;
        }

        set
        {
            movement = value;
        }
    }

    public void DamageEffect() {
        heroDamage.time = 0;
        heroDamage.Play();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ControlsTextView : MonoBehaviour
{

    public TextMeshProUGUI controlLabelsTextPro;
    public TextMeshProUGUI keysTextPro;

    [SerializeField]
    private List<string> controlsLabel;
    [SerializeField]
    private List<string> keyText;
    [SerializeField]
    private List<bool> controlsEnabled;
    StringBuilder stringBuilder = new StringBuilder();
    StringBuilder stringBuilder2 = new StringBuilder();

    public List<bool> ControlsEnabled
    {
        get
        {
            return controlsEnabled;
        }

        set
        {
            controlsEnabled = value;
        }
    }

    public List<string> ControlsLabel
    {
        get
        {
            return controlsLabel;
        }

        set
        {
            controlsLabel = value;
        }
    }

    public List<string> KeyText
    {
        get
        {
            return keyText;
        }

        set
        {
            keyText = value;
        }
    }

    // Use this for initialization
    void Start()
    {
        ShowText();
    }

    public void ShowText()
    {
        stringBuilder.Length = 0;
        stringBuilder2.Length = 0;
        for (int i = 0; i < ControlsLabel.Count; i++)
        {
            if (!ControlsEnabled[i]) continue;
            if (i > 0) {
                stringBuilder.AppendLine();
                stringBuilder2.AppendLine();
            } 
            stringBuilder.Append(ControlsLabel[i]);
            stringBuilder2.Append(KeyText[i]);

        }
        controlLabelsTextPro.text = stringBuilder.ToString();
        keysTextPro.text = stringBuilder2.ToString();
    }

    // Update is called once per frame
    void Update()
    {

    }
}

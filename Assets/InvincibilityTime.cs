﻿
using UnityEngine;
using System;

public class InvincibilityTime : MonoBehaviour {

    public float invincibleTimeOnHit;
    bool invincible;
    float invincibleCounter;

    public bool Invincible
    {
        get
        {
            return invincible;
        }

        set
        {
            invincible = value;
        }
    }

    public void HitHappen()
    {
        
        float invincibleTime = this.invincibleTimeOnHit;
        InvincibleState(invincibleTime);
    }

    public void InvincibleState(float invincibleTime)
    {
        invincible = true;
        if(invincibleTime > invincibleCounter)
            invincibleCounter = invincibleTime;
    }



    private void EndInvincibility()
    {
        invincible = false;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (invincibleCounter > 0) {
            invincibleCounter -= Time.deltaTime;
            if (invincibleCounter <= 0)
                EndInvincibility();
        }
	}
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventHolder : MonoBehaviour {

    public UnityEvent unityEvent;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    internal void Trigger()
    {
        Debug.Log("TRIGGER");
        unityEvent.Invoke();
    }
}

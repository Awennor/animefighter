﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioVolumeFixer : MonoBehaviour {

    public float volumeMultiplier;

    private AudioSource audioSource;
    private float originalVolume;

    // Use this for initialization
    void Start () {
        audioSource = GetComponent<AudioSource>();
        originalVolume = audioSource.volume;

	}
	
	// Update is called once per frame
	void Update () {
        audioSource.volume = originalVolume * (volumeMultiplier * volumeMultiplier * volumeMultiplier);
	}
}

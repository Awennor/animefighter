﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserIDGenerator {

    string key = "randomlyGeneratedUserID";
    private string userId;

    public UserIDGenerator()
    {
        bool haskey = PlayerPrefs.HasKey(key);
        if (!haskey) {
            string userIdNew = Random.Range(0, 999999)+"";
            PlayerPrefs.SetString(key, userIdNew);
            PlayerPrefs.Save();
        }
        string userId = PlayerPrefs.GetString(key);
        this.UserId = userId;
    }

    public string UserId
    {
        get
        {
            return userId;
        }

        set
        {
            userId = value;
        }
    }
}

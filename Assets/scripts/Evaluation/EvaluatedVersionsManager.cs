﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using UnityEngine;

public class EvaluatedVersionsManager
{
    EvaluatedVersionsData data;
    string key = "evaluatedversions";
    public EvaluatedVersionsManager() {
        if (PlayerPrefs.HasKey(key))
        {
            string json = PlayerPrefs.GetString(key);
            Debug.Log(json);
            data = JsonConvert.DeserializeObject<EvaluatedVersionsData>(json);
            //var bytes = MessagePackSerializer.FromJson(json);
            //data = MessagePackSerializer.Deserialize<EvaluatedVersionsData>(bytes);

        }
        else {
            Debug.Log("no key");
            data = new EvaluatedVersionsData();
        }
        
    }

    internal void Clear()
    {
        Debug.Log("Clear evaluated versions");
        data.EvaluatedVersions.Clear();
    }

    public void EvaluatedVersion(string version) {
        data.EvaluatedVersions.Add(version);
        //string value = MessagePackSerializer.ToJson<EvaluatedVersionsData>(data);
        string value = JsonConvert.SerializeObject(data);
        Debug.Log(value);
        PlayerPrefs.SetString(key, value);
        PlayerPrefs.Save();
    }

    public bool IsEvaluated(string version) {
        return data.EvaluatedVersions.Contains(version);
    }
}

[Serializable]
public class EvaluatedVersionsData {
    
    List<string> evaluatedVersions = new List<string>();

    public List<string> EvaluatedVersions
    {
        get
        {
            return evaluatedVersions;
        }

        set
        {
            evaluatedVersions = value;
        }
    }
}


﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class EvaluationController : MonoBehaviour {

    public TextAsset evaluationIds;
    public TextAsset evaluationQuestionTexts;
    public EvaluationModel evaluationModel;
    public EvaluationView evaluationView;
    public event Action OnEvaluationDone;


	// Use this for initialization
	void Start () {
        string evIds = evaluationIds.text;
        evIds = evIds.Replace("\r","");
        //var lines = Regex.Split(evIds, "\r\n|\r|\n");
        var lines = evIds.Split('\n');

        var evUnits = evaluationModel.EvaluationUnits;
        for (int i = 0; i < lines.Length; i++)
        {
            evUnits.Add(new EvaluationModel.EvaluationUnit(lines[i]));
        }
        string questions = evaluationQuestionTexts.text.Replace("\r", "");
        evaluationModel.AddQuestionTexts(questions.Split('\n'));

        evaluationView.ShowQuestions(evaluationModel.EvaluationQuestionTexts);
        evaluationView.OnReportAnswer += EvaluationView_OnReportAnswer;
        evaluationView.OnAllAnswered += EvaluationView_OnAllAnswered;
	}

    private void EvaluationView_OnAllAnswered()
    {
        if(OnEvaluationDone != null)
            OnEvaluationDone();
    }

    private void EvaluationView_OnReportAnswer(int arg1, int arg2)
    {
        evaluationModel.SetScore(questionId:arg1, answer:arg2);
    }

    // Update is called once per frame
    void Update () {
		
	}
}

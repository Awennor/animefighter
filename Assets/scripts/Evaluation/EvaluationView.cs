﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EvaluationView : MonoBehaviour {

    
    public List<EvaluationUnitUI> evUnits;
    public int amountPerPage;
    private List<string> questionTexts;
    int currentPage;
    public Button advanceButton;
    private int numberOfPages;
    public event Action OnAllAnswered;
    public bool onlyAdvanceWhenAllAnswered;
    public PlayableDirectorHelper pageTimeline;
    public event Action<int, int> OnReportAnswer;

    // Use this for initialization
    void Start () {
        advanceButton.onClick.AddListener(RequestAdvance);

	}

    private void RequestAdvance()
    {
        Report();
        currentPage++;
        if (numberOfPages > currentPage)
        {
            
            ShowPage(currentPage);
        }
        else {
            AllAnswered();
        }
        
    }

    private void Report()
    {
        for (int i = 0; i < evUnits.Count; i++)
        {
            int toggle = evUnits[i].WhichToggle();
            OnReportAnswer(i+currentPage*amountPerPage,toggle);
        }
    }

    private void AllAnswered()
    {
        if (OnAllAnswered != null)
            OnAllAnswered();
    }

    // Update is called once per frame
    void Update () {

        bool canAdvance = true;
        if (onlyAdvanceWhenAllAnswered) {
            foreach (var item in evUnits)
            {
                if (!item.ToggleGroup.AnyTogglesOn())
                {
                    canAdvance = false;
                    break;
                }
            }
        }
        advanceButton.gameObject.SetActive(canAdvance);

    }

    internal void ShowQuestions(List<string> evaluationQuestionTexts)
    {
        this.questionTexts = evaluationQuestionTexts;
        float totalAmountOfQuestions = evaluationQuestionTexts.Count;
        var r = totalAmountOfQuestions / amountPerPage;
        int ceil = (int)Math.Ceiling(r);
        numberOfPages = ceil;

        ShowPage(currentPage);

    }

    private void ShowPage(int currentPage)
    {
        pageTimeline.PlayFromStart();
        int initialQId = currentPage * amountPerPage;
        int length = initialQId + amountPerPage;
        if (length > questionTexts.Count) {
            length = questionTexts.Count;
        }
        AmountOfQuestions(length - initialQId);
        for (int i = initialQId; i < length; i++)
        {
            EvaluationUnitUI evaluationUnitUI = evUnits[i - initialQId];
            evaluationUnitUI.QuestionText.text = questionTexts[i];
            evaluationUnitUI.ToggleGroup.SetAllTogglesOff();
        }
    }

    private void AmountOfQuestions(int amountNecessary)
    {
        if (amountNecessary > amountPerPage)
            amountNecessary = amountPerPage;
        while (amountNecessary > evUnits.Count)
        {
            EvaluationUnitUI original = evUnits[0];
            EvaluationUnitUI evaluationUnitUI = Instantiate(original);
            
            evaluationUnitUI.transform.SetParent(original.transform.parent);
            evUnits.Add(evaluationUnitUI);
        }
    }
}

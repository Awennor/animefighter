﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvaluationModel : MonoBehaviour {

    [SerializeField]
    List<int> evaluationScores = new List<int>();
    [SerializeField]
    List<EvaluationUnit> evaluationUnits = new List<EvaluationUnit>();
    List<string> evaluationQuestionTexts = new List<string>();


    public List<EvaluationUnit> EvaluationUnits
    {
        get
        {
            return evaluationUnits;
        }

    }

    public List<string> EvaluationQuestionTexts
    {
        get
        {
            return evaluationQuestionTexts;
        }

    }

    public List<int> EvaluationScores
    {
        get
        {
            return evaluationScores;
        }

        set
        {
            evaluationScores = value;
        }
    }

    internal void AddQuestionTexts(string[] v)
    {
        evaluationQuestionTexts.AddRange(v);
    }

    //[SerializeField]
    //List<string> evaluationQuestionTexts = new List<string>();

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    [Serializable]
    public class EvaluationUnit {
        [SerializeField]
        string id;

        public EvaluationUnit(string id) {
            this.Id = id;
        }

        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }
    }

    internal void SetScore(int questionId, int answer)
    {
        Debug.Log(questionId+"qid");
        Debug.Log(answer + "qa");
        while (EvaluationScores.Count <= questionId) {
            EvaluationScores.Add(-1);
        }
        EvaluationScores[questionId] = answer;
    }
}

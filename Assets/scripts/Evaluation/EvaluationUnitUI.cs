﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EvaluationUnitUI : MonoBehaviour {
    [SerializeField]
    private ToggleGroup toggleGroup;
    [SerializeField]
    private TextMeshProUGUI questionText;
    private Toggle[] toggles;

    public ToggleGroup ToggleGroup
    {
        get
        {
            return toggleGroup;
        }

        set
        {
            toggleGroup = value;
        }
    }

    public TextMeshProUGUI QuestionText
    {
        get
        {
            return questionText;
        }

        set
        {
            questionText = value;
        }
    }



    // Use this for initialization
    void Start () {
        toggles = GetComponentsInChildren<Toggle>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    internal int WhichToggle()
    {
        for (int i = 0; i < toggles.Length; i++)
        {
            if (toggles[i].isOn) {
                return i;
            }
        }
        return -1;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class EvaluationApplication : MonoBehaviour {

    public EvaluationController evControl;
    public EvaluationModel evModel;
    StringBuilder stringBuilder = new StringBuilder();
    public bool debug;

    // Use this for initialization
    void Start () {
        evControl.OnEvaluationDone += EvControl_OnEvaluationDone;
	}

    private void EvControl_OnEvaluationDone()
    {
        var units = evModel.EvaluationUnits;
        var evScores = evModel.EvaluationScores;
        stringBuilder.Length = 0;
        stringBuilder.AppendLine("question$answer");
        for (int i = 0; i < units.Count; i++)
        {
            if(debug)
                Debug.Log("Evaluation Application save log "+units[i]+ " "+evScores[i]);
            stringBuilder.Append(units[i].Id);
            stringBuilder.Append('$');
            stringBuilder.Append(evScores[i]);
            stringBuilder.AppendLine();
        }
        CentralSingleton.Instance.SaveEvaluationLog(stringBuilder.ToString());
    }

    // Update is called once per frame
    void Update () {
		
	}
}

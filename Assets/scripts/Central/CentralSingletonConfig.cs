﻿using System;



class CentralSingletonConfig
{
    string softwareVersion = "proto1_b07a";
    string firebaseSecret = "eeYFCVruCyhqIOSDbntSaEhPKUFC2fToujCXrR95";
    string firebaseURL = "cubeproto.firebaseio.com";

    public CentralSingletonConfig() {
        EvaluationActive = false;
    }
    

    public string FirebaseSecret
    {
        get
        {
            return firebaseSecret;
        }

        set
        {
            firebaseSecret = value;
        }
    }

    public string FirebaseURL
    {
        get
        {
            return firebaseURL;
        }

        set
        {
            firebaseURL = value;
        }
    }

    public string SoftwareVersion
    {
        get
        {
            return softwareVersion;
        }

        set
        {
            softwareVersion = value;
        }
    }

    public bool EvaluationActive { get; internal set; }
}


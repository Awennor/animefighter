﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CentralSingleton
{
    static CentralSingleton instance;
    FirebaseConnectionManager firebaseConnection;
    CentralSingletonConfig config = new CentralSingletonConfig();
    FirebaseSave firebaseSave = new FirebaseSave();

    UserIDGenerator userId = new UserIDGenerator();
    EvaluatedVersionsManager evaluatedVersions = new EvaluatedVersionsManager();

    EvaluationSceneStartCondition evaluationCondition = new EvaluationSceneStartCondition();

    internal void BattleApplicationStarted(BattleApplication battleApplication)
    {
        evaluationCondition.BattleApplicationStarted(battleApplication);
        evaluationCondition.OnEvaluateRequested += EvaluationCondition_OnEvaluateRequested;
    }

    private void EvaluationCondition_OnEvaluateRequested()
    {
        bool isEvaluated = evaluatedVersions.IsEvaluated(firebaseSave.ApplicationVersion);

        if (!isEvaluated && config.EvaluationActive) {
            DOTween.KillAll();
            SceneManager.LoadScene("evaluationscene");
        }
            
    }

    public CentralSingleton() {
        string fs = config.FirebaseSecret;
        string furl = config.FirebaseURL;
        
        firebaseConnection = new FirebaseConnectionManager(fs, furl);
        firebaseSave.FireConnection = firebaseConnection;
        firebaseSave.UserId = userId.UserId;
        firebaseSave.saveOnEditor = true;
        firebaseSave.ApplicationVersion = config.SoftwareVersion;
    }

    internal void LogSystemIntegrationStarted(LogSystemIntegration logSystemIntegration)
    {
        logSystemIntegration.OnBattleEnd += firebaseSave.SaveBattleCSV;
        firebaseSave.BattleLogCapturer = logSystemIntegration.logCapturer;
    }



    public static CentralSingleton Instance
    {
        get
        {
            if (instance == null) {
                Debug.Log("Created central singleton");
                instance = new CentralSingleton();
            }
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    public EvaluatedVersionsManager EvaluatedVersions
    {
        get
        {
            return evaluatedVersions;
        }

        set
        {
            evaluatedVersions = value;
        }
    }

    internal void SaveEvaluationLog(string log)
    {
        evaluatedVersions.EvaluatedVersion(firebaseSave.ApplicationVersion);
        firebaseSave.SaveEvaluationLog(log);
        SceneManager.LoadScene("protoscene");
    }
}


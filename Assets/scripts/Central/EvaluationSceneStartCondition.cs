﻿using System;
using System.Collections.Generic;

public class EvaluationSceneStartCondition
{

    public event Action OnEvaluateRequested;
    int numberOfDeaths;
    int numberOfDeathsNecessary = 5;

    public void BattleApplicationStarted(BattleApplication bApp) {
        bApp.OnHeroDeath += BApp_OnHeroDeath;
        bApp.OnVictory += BApp_OnVictory;
    }

    private void BApp_OnVictory()
    {
        Request();
    }

    private void Request()
    {
        if (OnEvaluateRequested != null) {
            OnEvaluateRequested();
        }
    }

    private void BApp_OnHeroDeath()
    {
        numberOfDeaths++;
        if (numberOfDeaths >= numberOfDeathsNecessary) {
            Request();
        }
    }
}


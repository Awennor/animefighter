﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HealthEvents : MonoBehaviour {

    public Health health;
    private float lastRatio;
    public EventUnit[] units;

    // Use this for initialization
    void Start () {
        health.OnHealthChange += Health_OnHealthChange;
	}

    private void Health_OnHealthChange(int arg1, int arg2)
    {
        float ratio = health.HealthRatio.Value;
        
        foreach (var e in units)
        {
            var ts = e.RatioTriggers;
            foreach (var t in ts)
            {
                if (t > ratio && t <= lastRatio) {
                    e.Ue.Invoke();
                }
            }
        }
        lastRatio = ratio;
    }

    // Update is called once per frame
    void Update () {
		
	}
    [Serializable]
    public class EventUnit {
        [SerializeField]
        float[] ratioTriggers;
        [SerializeField]
        UnityEvent ue;

        public float[] RatioTriggers
        {
            get
            {
                return ratioTriggers;
            }

            set
            {
                ratioTriggers = value;
            }
        }

        public UnityEvent Ue
        {
            get
            {
                return ue;
            }

            set
            {
                ue = value;
            }
        }
    }
    
}

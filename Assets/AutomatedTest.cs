﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomatedTest : MonoBehaviour {
    static int testStage;
    static int loop;
    static bool testing;
    public Health hero, enemy;
    public InputHandler startButton;
    public InputHandler shootButton;
    public InputHandler autoShootButton;
    bool heroShoot;
    public BattleApplication battleApplication;
    public StartScreen start;
    public GameManager manager;

    [ContextMenu("Start Test")]
    public void Test()
    {
        testStage = 0;
        testing = true;
        loop = 0;
        FirstStage();
    }

    private void FirstStage()
    {
        var evaluated = CentralSingleton.Instance.EvaluatedVersions;
        evaluated.Clear();
        ActorHealth();
        startButton.ForceKeyPress();
        heroShoot = true;
        testStage++;
    }

    private void ActorHealth()
    {
        hero.maxHealth = 1;
        enemy.maxHealth = 1;
        hero.FullHealth();
        enemy.FullHealth();
    }

    private void ActorHealth_HeroLose()
    {
        hero.maxHealth = 1;
        enemy.maxHealth = 9999;
        hero.FullHealth();
        enemy.FullHealth();
    }

    public void Awake()
    {
        battleApplication.OnVictory += BattleApplication_OnVictory;
        battleApplication.OnHeroDeath += BattleApplication_OnHeroDeath;
    }

    private void BattleApplication_OnHeroDeath()
    {
        if (testStage == 3 && loop == 0)
        {
            DOVirtual.DelayedCall(0.5f, Stage3);
        }
    }

    private void Stage3()
    {
        autoShootButton.ForceKeyPress();
        loop += 1;
        testStage = 0;
        FirstStage();
    }

    private void BattleApplication_OnVictory()
    {
        if (testStage == 2)
        {
            DOVirtual.DelayedCall(0.5f, Stage2);

        }
    }

    private void Stage2()
    {
        ActorHealth_HeroLose();
        startButton.ForceKeyPress();
        heroShoot = false;
        testStage++;
    }

    // Use this for initialization
    void Start () {
        
        if (testing) {
            if (loop == 1) {
                autoShootButton.ForceKeyPress();
            }
            if (testStage == 1)
            {
                testStage++;
                DOVirtual.DelayedCall(0.5f, EnemyDeathTest);
            }

        }
	}

    private void EnemyDeathTest()
    {
        ActorHealth();
        startButton.ForceKeyPress();
        heroShoot = true;
        
    }

    // Update is called once per frame
    void Update () {
        if (heroShoot && enemy.CurrentHealth > 0) {
            if(shootButton.isActiveAndEnabled)
                shootButton.ForceKeyPress();
        }
	}

    public enum TestSteps {

    }
}

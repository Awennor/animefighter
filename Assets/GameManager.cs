﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class GameManager : MonoBehaviour {

    public PlayableDirector victoryTextTimeline;
    public Animator screenBlanket;
    public StartScreen startScreen;
    public Hittable hero;
    public GameObject enemy;
    public HeroDeath heroDeath;

    public event Action OnVictory;
    public event Action OnVictory_AnimationEnded;

    public ControlsTextView controlsTextView;
    public InputHandler shootInput;
        
    public void HeroAutoShoot(bool autoShoot) {
        Behavior behavior = hero.GetComponentInChildren<Behavior>();
        //behavior.autoActive = true;

            behavior.ActivateAndEnable(autoShoot);
        
            

        controlsTextView.ControlsEnabled[1] = !autoShoot;
        if(autoShoot)
            controlsTextView.ControlsLabel[3] = "AUTOSHOOT OFF";
        else
            controlsTextView.ControlsLabel[3] = "AUTOSHOOT ON";
        controlsTextView.ShowText();
        shootInput.gameObject.SetActive(!autoShoot);

    }

    public void Victory() {
        Debug.Log("victory");
        enemy.GetComponentInChildren<Behavior>().StopActing();
        hero.Active = false;
        victoryTextTimeline.time = 0;
        victoryTextTimeline.Play();
        if (OnVictory != null) {
            OnVictory();
        }
        DOVirtual.DelayedCall(4.5f, ()=> {
            screenBlanket.SetTrigger("fadeout");
            Debug.Log("victory2a");
            DOVirtual.DelayedCall(0.6f, ()=> {
                Debug.Log("victory2b");
                if (OnVictory_AnimationEnded != null)
                    OnVictory_AnimationEnded();
                if (startScreen != null) {
                    Debug.Log("victory2");
                    startScreen.BackToBeforeStart();
                    screenBlanket.SetTrigger("fadein");
                }
                else{
                    Debug.Log("victory3");
                }
                
            });
        });
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Playables;
using UniRx;
using System;
using DG.Tweening;

public class HeroDeath : MonoBehaviour
{

    public PlayableDirectorHelper heroDeath;
    //fadeInScreen;
    public GameObject heroView;
    public StartScreen startScreen;
    public Animator screenFade;
    public event Action OnHeroDeath;
    public float timeToRestart;
    public PlayableDirectorHelper heroRevive;

    public void StartDeath()
    {

        DOVirtual.DelayedCall(timeToRestart, RestartGame);
        //timelineDone;
        Debug.Log("DEATH");
        heroDeath.PlayFromStart();
        heroView.gameObject.SetActive(false);
        if (OnHeroDeath != null)
        {
            OnHeroDeath();
        }
    }

    private void RestartGame()
    {
        //Debug.Log("RESTART GAME");
        Observable.Timer(TimeSpan.FromSeconds(0.5f)).Subscribe(

            xs =>
            {
                Debug.Log("callback");
                if (screenFade != null) screenFade.SetTrigger("fadein");
                heroRevive.PlayFromStart();
                if (startScreen != null) startScreen.BackToBeforeStart();
            });
        DOVirtual.DelayedCall(1.5f, () =>
        {
            
        });
        screenFade.SetTrigger("fadeout");
        Debug.Log("RESTART GAME");

        //animator.SetTrigger("fadein");
        //heroDeath.Pause();
        //heroDeath.time = 0;


    }



    // Use this for initialization
    void Start()
    {
        bool previousFrame = false;

        //heroDeath.OnPlayableEndPlaying += RestartGame;


    }

    // Update is called once per frame
    void Update()
    {

    }
}

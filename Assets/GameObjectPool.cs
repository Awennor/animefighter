﻿using System;
using System.Collections.Generic;

using UnityEngine;


class UnityObjectPool<T> : Pool<T> where T:UnityEngine.Object
{

    T source;

    public T Source
    {
        get
        {
            return source;
        }

        set
        {
            source = value;
        }
    }

   

    public override T NewInstance()
    {
        if (source == null) return null;
        return GameObject.Instantiate(Source);
    }

    public void FillPool(int number, T prefab)
    {
        for (int i = 0; i < number; i++)
        {
            this.Inactive.Add(GameObject.Instantiate(prefab));
        }
    }

   
}

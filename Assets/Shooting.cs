﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public class Shooting : MonoBehaviour
{


    public GameObject target;
    public CollisionSystem collisionSystem;
    public float disMultiplier = 100;
    private Health health;
    public UnityEvent OnShoot;
    private Health healthTarget;
    public ShootingData defaultShootingData;
    public event Action<ShootingInfo> OnShootHappen;
    ShootingInfo shootingInfo = new ShootingInfo();
    public float toHitDelay;
    public float reactionTimeMultiplier = 1;

    public class ShootingInfo {

        GameObject shooter, target;
        int graphicRep;
        int position;
        float reactionTime;
        private int collisionId;

        public void SetAll(GameObject shooter, GameObject target, int graphicRep, int position, float reactionTime, int collisionId)
        {
            this.CollisionId = collisionId;
            this.shooter = shooter;
            this.target = target;
            this.graphicRep = graphicRep;
            this.position = position;
            this.reactionTime = reactionTime;
        }

        public GameObject Shooter
        {
            get
            {
                return shooter;
            }

        }

        public GameObject Target
        {
            get
            {
                return target;
            }


        }

        public int GraphicRep
        {
            get
            {
                return graphicRep;
            }

        }

        public int Position
        {
            get
            {
                return position;
            }

        }

        public float ReactionTime
        {
            get
            {
                return reactionTime;
            }

        }

        public int CollisionId
        {
            get
            {
                return collisionId;
            }

            set
            {
                collisionId = value;
            }
        }
    }

    // Use this for initialization
    void Start ()
    {
        this.healthTarget = target.GetComponent<Health>();
        health = GetComponent<Health>();
    }


    public void ShootIntention(ShootingData data, int position)
    {
        if (healthTarget.AboveZero())
        {
            Shoot(data, position);
        }
    }

    public void Shoot()
    {
        var shootingData = defaultShootingData;
        Shoot(shootingData);

    }

    private void Shoot(ShootingData shootingData)
    {
        
        Shoot(shootingData, -1);
    }

    private void Shoot(ShootingData shootingData, int position)
    {
        if (position == -1) {
            position = target.GetComponent<DigitalPosition>().Position;
        }
        if (gameObject != null && gameObject.activeSelf && health.AboveZero())
        {
            OnShoot.Invoke();

            if (!healthTarget.AboveZero() || !target.activeSelf)
            {
                //dis.y = 0;
                //dis.z = 0;
            }
            float reactionTime = shootingData.ReactionTime;


            int collisionId = collisionSystem.ScheduleCollision(position, reactionTime*reactionTimeMultiplier+toHitDelay, 0.04f);

            shootingInfo.SetAll(this.gameObject, target, shootingData.Graphic, position, reactionTime, collisionId);
            if (OnShootHappen != null)
                OnShootHappen(shootingInfo);
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}

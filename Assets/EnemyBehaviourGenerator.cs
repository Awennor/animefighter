﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviourGenerator : MonoBehaviour {
    [SerializeField]
    GeneratorUnit[] units;

	// Use this for initialization
	void Start () {
        Generate();
	}

    [ContextMenu("Generate")]
    private void Generate()
    {
        var data = GetComponent<EnemyBehaviourData>();
        data.ActionUnits.Clear();
        foreach (var gu in units)
        {
            for (int i = 0; i < gu.Loop; i++)
            {
                foreach (var p in gu.Pieces)
                {
                    for (int j = 0; j < p.Loop; j++)
                    {
                        data.ActionUnits.AddRange(p.ActionUnits);
                    }
                }
            }
            
        }
    }

    // Update is called once per frame
    void Update () {
		
	}

    [Serializable]
    public class GeneratorUnit {
        [SerializeField]
        GeneratorUnit_piece[] pieces;
        [SerializeField]
        int loop;

        public int Loop
        {
            get
            {
                return loop;
            }

            set
            {
                loop = value;
            }
        }

        public GeneratorUnit_piece[] Pieces
        {
            get
            {
                return pieces;
            }

            set
            {
                pieces = value;
            }
        }
    }

    [Serializable]
    public class GeneratorUnit_piece {
        [SerializeField]
        EnemyBehaviourData.ActionUnit[] actionUnits;
        [SerializeField]
        int loop;

        public EnemyBehaviourData.ActionUnit[] ActionUnits
        {
            get
            {
                return actionUnits;
            }

            set
            {
                actionUnits = value;
            }
        }

        public int Loop
        {
            get
            {
                return loop;
            }

            set
            {
                loop = value;
            }
        }
    }
}

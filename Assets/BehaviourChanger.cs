﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class BehaviourChanger : MonoBehaviour
{

    public ChangeUnit[] changeUnits;
    public Behavior behavior;
    public Health health;
    public Renderer enemyView;
    public bool changeMaterialColor;

    // Use this for initialization
    void Start()
    {
        health.HealthRatio.Subscribe(ChangeHPRatio);
        ChangeHPRatio(1);
    }

    public void ChangeHPRatio(float currentRatio)
    {
        if (!isActiveAndEnabled) {
            return;
        }
        if (currentRatio == 0) return;
        //Debug.Log(currentRatio + " HP RATIO");
        ChangeUnit chosen = null;
        

        foreach (var cu in changeUnits)
        {
            var hp = cu.HpRatio;

            if (hp >= currentRatio)
            {
                if (chosen == null)
                {
                    chosen = cu;
                }
                else
                {
                    if (cu.HpRatio < chosen.HpRatio)
                    {
                        chosen = cu;
                    }
                }
            }
        }
        if (chosen != null && behavior.BehaviourData != chosen.BehaviorData)
        {
            behavior.BehaviourData = chosen.BehaviorData;
            if(changeMaterialColor)
                enemyView.material.color = chosen.Color;
            if(chosen.PlayableDirector != null) chosen.PlayableDirector.PlayFromStart();
            
        }

    }

    [Serializable]
    public class ChangeUnit
    {
        [SerializeField]
        float hpRatio;
        [SerializeField]
        EnemyBehaviourData behaviorData;
        [SerializeField]
        Color color;
        [SerializeField]
        PlayableDirectorHelper playableDirector;

        public float HpRatio
        {
            get
            {
                return hpRatio;
            }

            set
            {
                hpRatio = value;
            }
        }

        public EnemyBehaviourData BehaviorData
        {
            get
            {
                return behaviorData;
            }

            set
            {
                behaviorData = value;
            }
        }

        public Color Color
        {
            get
            {
                return color;
            }

            set
            {
                color = value;
            }
        }

        public PlayableDirectorHelper PlayableDirector
        {
            get
            {
                return playableDirector;
            }

            set
            {
                playableDirector = value;
            }
        }
    }
}

﻿using Pidroh.UnityUtils.LogDataSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class FirebaseSave {

    private FirebaseConnectionManager fireConnection;
    LogCapturerManager log;
    string userId;
    private string keyBattle;
    public bool saveOnEditor;
    private string applicationVersion;

    public string UserId
    {
        get
        {
            return userId;
        }

        set
        {
            userId = value;
        }
    }

    public FirebaseConnectionManager FireConnection
    {

        set
        {
            fireConnection = value;
        }
    }

    public LogCapturerManager BattleLogCapturer
    {


        set
        {
            log = value;
        }
    }

    public string ApplicationVersion
    {
        get
        {
            return applicationVersion;
        }

        set
        {
            applicationVersion = value;
        }
    }

    public void SaveBattleCSV()
    {
        if (log == null) return;
        string pathAppend = "/battlecsv";
        if(keyBattle == null) //reuses key because it can save multiple times
            keyBattle = GenerateKey(pathAppend);
        var logDatas = log.LogDatas;
        var csv = LogConverter.ToCSV(logDatas, '$');
        Debug.Log(csv + "data");
        Save(keyBattle, csv);

    }

    private string GenerateKey(string pathAppend)
    {
        string userIdSuffix = "";
        if (Application.isEditor)
        {
            userIdSuffix += "editor";
        }
        string key = "AnimeFighter/" + "/" + ApplicationVersion + "/" + UserId + userIdSuffix + "/" + pathAppend+"/" + DateTime.Now.ToString().Replace("/", "_") ;
        return key;
    }

    private void Save(string key, string data)
    {
        if (Application.isEditor)
        {
            if (!saveOnEditor)
                return;
        }

        Debug.Log("Save!");

        fireConnection.SaveInfo(data, key);
    }

    internal void SaveEvaluationLog(string data)
    {
        string pathAppend = "/evaluationcsv";
        
        Debug.Log(data + "data");
        Save(GenerateKey(pathAppend), data);
    }

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using SimpleFirebaseUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FirebaseTest : MonoBehaviour {
    private Firebase firebase;
    public FirebaseConnectionMonoBehaviour firebaseConnection;

    [ContextMenu("test firebase2")]
    public void TestFireBase2() {
        firebaseConnection.SaveInfo("blabli", "whatever");
    }

    [ContextMenu("test firebase")]
    public void TestFirebase() {
        
        Debug.Log("Create new now!");
        if (firebase == null) {
            firebase = Firebase.CreateNew("cubeproto.firebaseio.com", "eeYFCVruCyhqIOSDbntSaEhPKUFC2fToujCXrR95");
            Debug.Log("Create new now!"+firebase);
        }
            
        Debug.Log(firebase.Host+"___Firebase host");
        
        firebase.OnSetSuccess += OnSetSuccess;
        firebase.OnSetFailed += OnSetFail;
        firebase.Child("blablu", true).SetValue("{\"alanisawesome\": {\"name\": \"Alan Turing\", \"birthday\": \"June 23, 1912\"}}");
        

    }

    private void OnSetFail(Firebase arg1, FirebaseError arg2) {
        Debug.Log("SET FAIL!!!" + arg2.Message + arg2.InnerException);
    }

    private void OnSetSuccess(Firebase arg1, DataSnapshot arg2) {
        Debug.Log("SET SUCCESS!!!");
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HeroMovement : MonoBehaviour {

    public GameObject hero;

    public Transform[] pos;
    int currentPosition = 0;
    private DigitalPosition digitalPos;
    private Health health;
    public UnityEvent OnMove;

    public void MoveToPos(int which) {
        if (health.AboveZero()) {
            OnMove.Invoke();
            currentPosition = which;
            hero.transform.position = pos[which].position;
            digitalPos.Position = which;
        }
        
    }

    internal Vector3 GetPosition(int position)
    {
        return pos[position].position;
    }

    public void Alternate() {
        if (digitalPos.Position == 1)
        {
            MoveToPos(0);
        }
        else {
            MoveToPos(1);
        }
    }

	// Use this for initialization
	void Start () {
        digitalPos = hero.GetComponent<DigitalPosition>();
        health = hero.GetComponent<Health>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

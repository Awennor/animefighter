﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingData : MonoBehaviour {
    [SerializeField]
    float reactionTime;
    [SerializeField]
    int graphic;

    public float ReactionTime
    {
        get
        {
            return reactionTime;
        }

        set
        {
            reactionTime = value;
        }
    }

    public int Graphic
    {
        get
        {
            return graphic;
        }

        set
        {
            graphic = value;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeSpawner : MonoBehaviour {

    public GameObject prefab;
    UnityObjectPool<GameObject> pool;

    public float spawnSphericalVariance;

    public Vector2 amount;
    int amountMin;
    int amountMax;
    public float maxStrength;
    public float fixedStrength;
    public Vector2 scale;
    public bool spawnOnEnable;
    public int amountInPool = 25;

    // Use this for initialization
    void Awake () {
        pool = new UnityObjectPool<GameObject>();
        pool.FillPool(amountInPool, prefab);
	}

	void OnEnable(){
		if(spawnOnEnable){
			Spawn();
            Debug.Log("Spawn!");
        }
	}

    public void FreeAll() {
        pool.DesactivateAll();
    }

    [ContextMenu("spawn")]
    public void Spawn() {
        for (int i = 0; i < Random.Range(amount.x, amount.y); i++)
        {
            if (!pool.HasInactive) {
                pool.Desactive(5);
            }
            GameObject obj = pool.ActivateObject();
            obj.transform.localScale = Vector3.one * Random.Range(scale.x, scale.y);
            obj.transform.position = this.transform.position + Random.insideUnitSphere * spawnSphericalVariance;
            obj.SetActive(true);
            var body = obj.GetComponent<Rigidbody>();
            body.AddForce(Random.insideUnitSphere*maxStrength+ Random.onUnitSphere * fixedStrength);

        }
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

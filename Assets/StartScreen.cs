﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StartScreen : MonoBehaviour {

    public GameObject enemy;
    public GameObject hero;
    public GameObject heroView;
    public GameObject enemyView;
    public GameObject controls;
    bool gameStarted;
    public event Action OnBattleStart;
    
    public List<GameObject> ActiveOnStartScreen;

    public UnityEvent OnStart;

    public void StartGame() {
        if (!gameStarted) {
            if (OnBattleStart != null) {
                OnBattleStart();
            }
            for (int i = 0; i < ActiveOnStartScreen.Count; i++)
            {
                ActiveOnStartScreen[i].gameObject.SetActive(false);

            }
            gameStarted = true;
            enemy.SetActive(true);
            enemyView.SetActive(true);
            enemy.GetComponent<Health>().FullHealth();
            enemy.GetComponent<Hittable>().Active = true;
            hero.GetComponent<Hittable>().Active = true;
            //controls.SetActive(false);
            enemy.GetComponentInChildren<Behavior>().StartActing();
            OnStart.Invoke();
        }
    }

    public void BackToBeforeStart() {
        gameStarted = false;
        hero.GetComponent<Health>().FullHealth();
        enemy.GetComponent<Health>().CurrentHealth = 0;
        enemy.GetComponent<Hittable>().Active = false;
        enemyView.SetActive(false);
        //enemy.SetActive(false);
        heroView.SetActive(true);
        controls.SetActive(true);
        controls.GetComponent<Animator>().SetTrigger("arrive");
        Behavior behavior = enemy.GetComponentInChildren<Behavior>(true);
        behavior.StopActing();

        for (int i = 0; i < ActiveOnStartScreen.Count; i++)
        {
            ActiveOnStartScreen[i].gameObject.SetActive(true);

        }
    }

	// Use this for initialization
	void Start () {
        BackToBeforeStart();
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

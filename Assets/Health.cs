﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour {

    public int maxHealth;
    [SerializeField]
    private int currentHealth;
    public ReactiveProperty<int> ReactiveHealth { get; private set; }
    public ReactiveProperty<float> HealthRatio { get; private set; }

    public int CurrentHealth
    {
        get
        {
            return currentHealth;
        }

        set
        {
            currentHealth = value;
        }
    }

    //ReactiveProperty<int> CurrentHealth;
    public UnityEvent OnHealthZero;
    public event Action<int, int> OnHealthChange;

    public void Awake()
    {
        ReactiveHealth = new ReactiveProperty<int>();
        HealthRatio = ReactiveHealth.Select(x => ((float)currentHealth) / ((float)maxHealth)).ToReactiveProperty();
    }

    public void FullHealth() {
        HealthChange(maxHealth);
    }

    public void HealthChange(int change) {
        bool wasAboveZero = currentHealth > 0;
        currentHealth += change;
        if (currentHealth > maxHealth) currentHealth = maxHealth;
        if (currentHealth <= 0) {
            currentHealth = 0;

            if(wasAboveZero)
                OnHealthZero.Invoke();
        }
        if(OnHealthChange != null)
            OnHealthChange(currentHealth, change);
        ReactiveHealth.Value = currentHealth;
    }

    internal bool AboveZero()
    {
        return currentHealth > 0;
    }
}

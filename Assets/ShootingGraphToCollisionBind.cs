﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingGraphToCollisionBind : MonoBehaviour {

    public CollisionSystem[] collisionSystems;
    public ShootingGraphics shootingGraphic;

	// Use this for initialization
	void Start () {
        foreach (var item in collisionSystems)
        {
            item.OnCollisionHappened_Id += shootingGraphic.CollisionHappen;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

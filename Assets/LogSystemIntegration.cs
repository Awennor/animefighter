﻿using Pidroh.UnityUtils.LogDataSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class LogSystemIntegration : MonoBehaviour {

    public GameObject hero, enemy;
    public StartScreen startScreen;
    public GameManager gameManager;
    public LogCapturerManager logCapturer;
    public event Action OnBattleEnd;

	// Use this for initialization
	void Start () {
        CentralSingleton.Instance.LogSystemIntegrationStarted(this);
        //Debug.Log("software version");
        startScreen.OnBattleStart += StartScreen_OnBattleStart;
        gameManager.OnVictory_AnimationEnded += () =>
        {
            logCapturer.DirectLog("Victory");
            if (OnBattleEnd != null) {
                OnBattleEnd();
            }
        };
        hero.GetComponent<Shooting>().OnShootHappen += (s) => {
            logCapturer.DirectLog("HeroShoot");
        };
        enemy.GetComponent<Shooting>().OnShootHappen += (s) => {
            logCapturer.DirectLog("EnemyShoot", s.ReactionTime+"");
        };
        Health heroHealth = hero.GetComponent<Health>();
        heroHealth.OnHealthChange += (health, changed) =>
        {
            if (changed < 0) {
                logCapturer.DirectLog("HeroDamage", changed+"");
            }
        };
        heroHealth.OnHealthZero.AddListener(()=> {
            logCapturer.DirectLog("HeroDeath");
            if (OnBattleEnd != null)
            {
                OnBattleEnd();
            }
        });
        Behavior behavior = enemy.GetComponentInChildren<Behavior>();
        ReactiveProperty<EnemyBehaviourData> reactiveBehavior = behavior.ReactiveBehavior;
        
        reactiveBehavior.Subscribe((b)=> {
            if(b != null)
                logCapturer.DirectLog("EnemyBehaviorChange", b.name); 
        });
        //enemy.GetComponent<Health>().OnHealthChange += (health, changed) =>
        //{
        //    if (changed < 0)
        //    {
        //        logCapturer.DirectLog("EnemyDamage", changed + "");
        //    }
        //};
        hero.GetComponent<DigitalPosition>().OnPositionChanged += (pos) =>
        {
            logCapturer.DirectLog("HeroMove");
        };
    }

    private void StartScreen_OnBattleStart()
    {
        logCapturer.DirectLog("BattleStart");
    }

    // Update is called once per frame
    void Update () {
		
	}
}

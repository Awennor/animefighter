﻿using UnityEngine;
using System.Collections;

/**
Dirty, quick, approximation of an Entity-ID in C# :
...in reality, this is nothing more or less than a wrapper for System.Guid.

In most C, C++, etc languages, this would be #defined as System.Guid, and
later replaced with a full struct/class with more features/performance - but Unity's C# won't allow that.
*/
[System.Serializable]
public struct EntityID
{
    public readonly System.Guid guid;

    public EntityID(object fakeObjectThatIsAlwaysIgnoredManIHateCSharp)
    {
        guid = System.Guid.NewGuid();
    }

    public static EntityID New()
    {
        return new EntityID(null);
    }

    public static bool operator ==(EntityID e1, EntityID e2)
    {
        return e1.guid == e2.guid;
    }
    public static bool operator !=(EntityID e1, EntityID e2)
    {
        return e1.guid != e2.guid;
    }

    public override bool Equals(object obj)
    {
        return guid.Equals(((EntityID)obj).guid);
    }

    public override string ToString()
    {
        return guid.ToString();
    }
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SomeComponents : MonoBehaviour
{
    /**
	This is a scratchpad where I noted down thoughts on "what's the simplest-possible approximation
	of an ECS in C# that's interface-compatible with a fast, performance implementation later on, but
	allows me to write this in as few lines of code as possible to make this tutorial go live?"
	
	...some of this will get re-used / expanded upon in later tutorials. I leave it here for your interest
	only - see what I kept and what I removed!
	
	----
	
	Queries we need:
	
	A. EID + type -> value-object
	B. EID -> all (type[key] + value-object) pairs --- we'll then use the type to pick specific value-object's to read
	C. type -> all eid's
	D. * -> all (eid + all (type[key] + value-object) pairs ) pairs --- we'll then use the type to pick specific value-object's to read
	E. EID -> remove: type
	F. EID -> exists / does not exist
	
	Queries used by each method:
	
	- EntityID : create() : F
	- valueobject : get<velocity>( eid ) : A
	- add<velocity>( eid, new velocity ) : A
	- ? : selectAllWith( types[] ) : C (pre-filter eid's), D (store results for iteration)
	- valueobject : remove<velocity>( eid ) : E
	
	And, soon:
	- refreshSelectAllWith( types[], PreviousResultOfThisCall ) ... hmm.
	
	Therefore, our minimal set of ADT's to store all this is:
	
	- "PartialEntity" {eid + Dictionary<type,valueobject> } -- may have SOME, NONE, or ALL of its components included here
	- allEntities: Dictionary<eid, List<PartialEntity>>
	- componentsOnEntitiesLookupTable: Dictionary<type, List<eid>>
	- (result of selectAllWith( types[] )) : List<PartialEntity>
	
	*/

    /**
	By nature, the allMappedComponentsByType structure is the "wrong way around" for you to
	ask which components an Entity possesses - a common query we need!
	
	So, in all simple ECS implementations, you want a cache of the reverse-direction, so you can do that
	lookup fast
	*/
    public Dictionary<System.Type, List<EntityID>> fastLookupEntitiesPossessingComponent = new Dictionary<System.Type, List<EntityID>>();

#if USE_PARTIALENTITY_CLASS
	
	NB: because I decided we don't need the PartialEntity class, I commented this out completely
	
	public class PartialEntity
	{
	public EntityID eid;
	private Dictionary<System.Type, object> _componentsByType;
	public T component<T>()
	{
		return (T) _componentsByType[ typeof(T) ];
	}
	}
	
	public Dictionary<EntityID, List<PartialEntity>> allEntitiesByID = new Dictionary<EntityID, List<PartialEntity>>();
	
#else

    public Dictionary<System.Type, Dictionary<EntityID, object>> allMappedComponentsByType = new Dictionary<System.Type, Dictionary<EntityID, object>>();
    public List<EntityID> allEntitiesByID = new List<EntityID>();

    public EntityID createEntity()
    {
        EntityID eid = EntityID.New();
        allEntitiesByID.Add(eid);
        return eid;
    }

    public List<EntityID> getAllEntityIDs()
    {
        return allEntitiesByID;
    }

    /** I named it "editable" to make clear you are getting a direct reference you can overwrite.
    
    In C#, you often receive items by value - your writes are discarded! - and there's no way of
    telling which you've received by looking at it. Naming conventions help here.
    
    Also, in particular for these tutorials: it will soon make a HUGE difference whether we want
    writeable or read-only access when calling this method
    */
    public T editable<T>(EntityID eid) where T : class
    {
        if (!fastLookupEntitiesPossessingComponent.ContainsKey(typeof(T)))
            return null;
        if (!fastLookupEntitiesPossessingComponent[typeof(T)].Contains(eid))
            return null;

        T result = (T)allMappedComponentsByType[typeof(T)][eid];

        return result;
    }

    /** Add a component to an entity. Sometimes called "add", but I find that name too generic */
    public void attach<T>(EntityID eid, T newValue)
    {
        PreAllocateStructuresForType<T>();

        allMappedComponentsByType[typeof(T)][eid] = newValue;

        List<EntityID> eidsWithThisComponent = fastLookupEntitiesPossessingComponent[typeof(T)];
        eidsWithThisComponent.Add(eid);
    }

    public T detach<T>(EntityID eid)
    {
        fastLookupEntitiesPossessingComponent[typeof(T)].Remove(eid);

        T result = (T)allMappedComponentsByType[typeof(T)][eid];
        allMappedComponentsByType[typeof(T)][eid] = null;
        return result;
    }

    /** Common for C# (and most C-derivative) ECS's: a need to warn the ECS that it needs to
    pre-allocate space for a new Component added to the system. I've added it here mostly for
    forwards-compatibility.
    
    Other languages don't need this, but C languages often need you to allocate early so that
    you can do efficient memory-management and data-structure layout in memory
    */
    private void PreAllocateStructuresForType<T>()
    {
        if (!allMappedComponentsByType.ContainsKey(typeof(T)))
        {
            Debug.Log("No list allocated for type = " + typeof(T) + " yet; pre-allocating now");
            allMappedComponentsByType[typeof(T)] = new Dictionary<EntityID, object>();
        }
        if (!fastLookupEntitiesPossessingComponent.ContainsKey(typeof(T)))
        {
            fastLookupEntitiesPossessingComponent[typeof(T)] = new List<EntityID>();
        }
    }
#endif
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyChanger : MonoBehaviour {

    public BehaviourChanger[] changers;
    public DifficultyData[] datas;
    int difficulty;
    public Health enemyHealth;
    public event Action<DifficultyData> OnDifficultyChange;
    public ControlsTextView controlsTextView;

    public void IncreaseDifficulty()
    {
        difficulty++;
        if (difficulty >= datas.Length)
        {
            difficulty = 0;
        }
        int d = difficulty;
        ChangeDifficulty(d);
    }

    private void ChangeDifficulty(int d)
    {
        Debug.Log("CHANGE D "+d);
        for (int i = 0; i < changers.Length; i++)
        {
            changers[i].gameObject.SetActive(false);
        }
        Debug.Log("CHANGE D " + datas[d].Label);
        datas[d].Behaviour.gameObject.SetActive(true);
        enemyHealth.maxHealth = datas[d].Hp;
       
        if (OnDifficultyChange != null)
        {
            OnDifficultyChange(datas[d]);
        }
        controlsTextView.ControlsLabel[4] = string.Format("CHANGE DIFFICULTY ({0}) ", datas[d].Label);
        controlsTextView.ShowText();
    }

    // Use this for initialization
    void Start () {
        ChangeDifficulty(0);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    [Serializable]
    public struct DifficultyData {
        [SerializeField]
        BehaviourChanger behaviour;
        [SerializeField]
        int hp;
        [SerializeField]
        string label;

        public BehaviourChanger Behaviour
        {
            get
            {
                return behaviour;
            }

            set
            {
                behaviour = value;
            }
        }

        public int Hp
        {
            get
            {
                return hp;
            }

            set
            {
                hp = value;
            }
        }

        public string Label
        {
            get
            {
                return label;
            }

            set
            {
                label = value;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectilView : MonoBehaviour {

    public Renderer render;
    int collisionId;

    public int CollisionId
    {
        get
        {
            return collisionId;
        }

        set
        {
            collisionId = value;
        }
    }



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

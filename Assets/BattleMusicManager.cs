﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleMusicManager : MonoBehaviour {

    public AudioSource battleMusic1;
    public AudioSource battleMusic2;
    StartScreen startScreen;
    public BattleApplication gm;
    public EventHolder musicStop;

    // Use this for initialization
    void Start () {
        gm.OnVictory += Gm_OnVictory;
        gm.OnHeroDeath += Gm_OnHeroDeath;
        //startScreen.OnBattleStart += StartScreen_OnBattleStart;
        //gm.
	}

    private void Gm_OnHeroDeath()
    {
        musicStop.Trigger();
    }

    private void Gm_OnVictory()
    {
        musicStop.Trigger();
    }

    private void StartScreen_OnBattleStart()
    {
        Activate(battleMusic1);
    }

    private void Activate(AudioSource battleMusic1)
    {
        throw new NotImplementedException();
    }

    // Update is called once per frame
    void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderQueueModifier : MonoBehaviour {

    int que = -1;

	// Use this for initialization
	void Start () {
        GetComponent<Renderer>().material.renderQueue = 1;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

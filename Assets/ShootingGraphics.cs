﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingGraphics : MonoBehaviour {

    public Shooting[] shooting;
    public ViewBinder[] viewBinders;
    public ProjectilView[] proj;
    UnityObjectPool<ProjectilView>[] projPools;

    // Use this for initialization
    void Start()
    {
        foreach (var s in shooting)
        {
            s.OnShootHappen += S_OnShootHappen;
        }
        projPools = new UnityObjectPool<ProjectilView>[proj.Length];
        for (int i = 0; i < projPools.Length; i++)
        {
            projPools[i] = new UnityObjectPool<ProjectilView>();
            projPools[i].Source = proj[i];


        }
    }

    private void S_OnShootHappen(Shooting.ShootingInfo obj)
    {
        var pView = Shoot(obj.Shooter, obj.Target, obj.GraphicRep, obj.ReactionTime, obj.Position);
        pView.CollisionId = obj.CollisionId;

    }

    public void CollisionHappen(int collisionId) {
        for (int i = 0; i < projPools.Length; i++)
        {
            var active = projPools[i].Active;
            for (int j = 0; j < active.Count; j++)
            {
                var a = active[j];
                if (a.CollisionId == collisionId)
                {
                    Deactivate(projPools[i], a);
                }
            }

        }
    }

    public ProjectilView Shoot(GameObject shooter, GameObject target, int shootGraphic, float reactionTime, int position) {
        var view = GetView(shooter);
        var viewTarget = GetView(target);
        var initialPosition = view.transform.position;
        Vector3 finalPosition = viewTarget.Movement.GetPosition(position);

        var newProj = projPools[shootGraphic].ActivateObject();//Instantiate(proj[shootGraphic]);
        newProj.gameObject.SetActive(true);
        newProj.transform.position = initialPosition;
        var dis = finalPosition - initialPosition;
        float disMultiplier = 100;
        
        newProj.transform.DOBlendableMoveBy(dis * disMultiplier, reactionTime * disMultiplier).SetEase(Ease.Linear);
        return newProj;
    }

    private ActorView GetView(GameObject shooter)
    {
        foreach (var vb in viewBinders) {
            if (vb.model == shooter)
                return vb.view;
        }
        return null;
    }


	
	// Update is called once per frame
	void Update () {
        foreach (var p in projPools)
        {
            var active = p.Active;
            for (int i = 0; i < active.Count; i++)
            {
                var a = active[i];
                if (!a.render.isVisible)
                {
                    //Debug.Log("desactivate!");
                    Deactivate(p, a);
                    i--;
                }
                else {
                    //Debug.Log("activate!");
                }

            }

        }
	}

    private static void Deactivate(UnityObjectPool<ProjectilView> p, ProjectilView a)
    {
        a.gameObject.SetActive(false);
        p.DesactivateObject(a);
        a.DOKill();
        a.transform.DOKill();
    }

    [Serializable]
    public class ViewBinder {
        public ActorView view;
        public GameObject model;
    }

}

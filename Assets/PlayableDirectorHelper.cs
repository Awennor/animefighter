﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public static class PlayableDirectorExtensions {
    public static void PlayFromStart(this PlayableDirector pd) {
        pd.time = 0;
        pd.Play();
    }
}

public class PlayableDirectorHelper : MonoBehaviour {
    private PlayableDirector playableDirector;
    bool overtimeLastFrame;
    public event Action OnPlayableEndPlaying;

    public PlayableDirector PlayableDirector
    {
        get
        {
            return playableDirector;
        }

        set
        {
            playableDirector = value;
        }
    }

    public void PlayFromStart() {
        if (!enabled || !gameObject.activeSelf) return;
        PlayableDirector.Pause();
        PlayableDirector.time = 0;
        PlayableDirector.Play();
    }

    // Use this for initialization
    void Awake () {
        this.PlayableDirector = GetComponent<PlayableDirector>();
	}
	
	// Update is called once per frame
	void Update () {
        bool overtime = playableDirector.time >= playableDirector.duration;
        if (overtime && !overtimeLastFrame) {
            if(OnPlayableEndPlaying != null)
                OnPlayableEndPlaying();
        }
        overtimeLastFrame = overtime;
	}

    internal void Pause()
    {
        playableDirector.Pause();
    }
}

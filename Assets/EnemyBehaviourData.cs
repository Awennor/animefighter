﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviourData : MonoBehaviour {

    [SerializeField]
    List<ActionUnit> actionUnits;

    public List<ActionUnit> ActionUnits
    {
        get
        {
            return actionUnits;
        }

        set
        {
            actionUnits = value;
        }
    }

    [Serializable]
    public class ActionUnit {
        [SerializeField]
        BehaviourTypes bt;
        [SerializeField]
        int argInt;
        [SerializeField]
        int argInt2;
        [SerializeField]
        float argFloat;

        public BehaviourTypes Bt
        {
            get
            {
                return bt;
            }

            set
            {
                bt = value;
            }
        }

        public int ArgInt
        {
            get
            {
                return argInt;
            }

            set
            {
                argInt = value;
            }
        }

        public float ArgFloat
        {
            get
            {
                return argFloat;
            }

            set
            {
                argFloat = value;
            }
        }

        public int ArgInt2
        {
            get
            {
                return argInt2;
            }

            set
            {
                argInt2 = value;
            }
        }
    }

    internal bool ContainsIndex(int index)
    {
        return ActionUnits.Count > index;
    }

    internal ActionUnit GetActionUnit(int index)
    {
        return ActionUnits[index];
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

public enum BehaviourTypes {
    Shoot = 0, AimPosition, Wait = 2
}
